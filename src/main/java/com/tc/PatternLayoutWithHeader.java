package com.tc;

import org.apache.log4j.PatternLayout;

public class PatternLayoutWithHeader extends PatternLayout {

    @Override
    public String getHeader() {
        return "HEADER" + System.lineSeparator() + "------" + System.lineSeparator() + System.lineSeparator();
    }

}
