package com.tc;

import org.apache.log4j.Logger;

/**
 * @author tc
 * 
 * one way to create rolling log files with a header
 * 
 */
public class HelloExample {

    final static Logger logger = Logger.getLogger(HelloExample.class);

    public static void main(String[] args) {
        HelloExample helloExample = new HelloExample();
        int counter = 20;
        for (int i = 0; i < counter; i++) {
            helloExample.run("tc");
        }
    }

    private void run(String parameter) {
        if (logger.isDebugEnabled()) {
            logger.debug("This is debug : " + parameter);
        }
        if (logger.isInfoEnabled()) {
            logger.info("This is info : " + parameter);
        }
        logger.warn("This is warn : " + parameter);
        logger.error("This is error : " + parameter);
        logger.fatal("This is fatal : " + parameter);
    }

}
